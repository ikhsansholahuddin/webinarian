part of 'models.dart';

class AuthSignIn{
  String email, password;

  AuthSignIn({this.email, this.password});

  Map<String, dynamic> toJson(){
    return {"email": email, "password": password};
  }

  @override
  String toString(){
    return 'AuthSignIn{email: $email, password: $password}';
  }
}

String authSignInToJson(AuthSignIn data){
  final jsonData = data.toJson();
  return json.encode(jsonData);
}