part of 'models.dart';

class RegistrationData{
  String name, email, password;

  RegistrationData({this.name = "", this.email = "", this.password = ""});
}