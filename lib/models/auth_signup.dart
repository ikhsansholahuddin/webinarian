part of 'models.dart';

class AuthSignUp{
  String name, email, password;

  AuthSignUp({this.name, this.email, this.password});

  Map<String, dynamic> toJson(){
    return {"name": name, "email": email, "password": password};
  }

  @override
  String toString() {
    return 'AuthSignUp{name: $name, email: $email, password: $password}';
  } 
}

String authSignUpToJson(AuthSignUp data){
  final jsonData = data.toJson();
  return json.encode(jsonData);
}