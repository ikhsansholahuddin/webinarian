import 'dart:convert';
import 'package:equatable/equatable.dart';

part 'auth_signup.dart';
part 'auth_signin.dart';
part 'registration_data.dart';
part 'user.dart';