import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:equatable/equatable.dart';
import 'package:webinarian/bloc/blocs.dart';
import 'package:webinarian/models/models.dart';

part 'page_event.dart';
part 'page_state.dart';

class PageBloc extends Bloc<PageEvent, PageState> {
  @override
  PageState get initialState => OnInitialPage();

  @override
  Stream<PageState> mapEventToState(
    PageEvent event,
  ) async* {
    if(event is GoToSplashPage){
      yield OnSplashPage();
    }
    else if(event is GoToSignInPage){
      yield OnSignInPage();
    }
    else if(event is GoToSignUpPage){
      yield OnSignUpPage(event.registrationData);
    }
    else if(event is GoToMainPage){
      yield OnMainPage();
    }
    else if(event is GoToTicketDetailPage){
      yield OnTicketDetailPage();
    }
    else if(event is GoToRegisterPage){
      yield OnRegisterPage();
    }
  }
}
