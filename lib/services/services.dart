import 'dart:convert';

import 'package:http/http.dart' show Client;
import 'dart:async';
import 'package:webinarian/models/models.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ApiService{
  final String baseUrl = "http://103.15.226.142/webinarian";
  Client client = Client();

  bool signInAuth;

  saveUsername(String username) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();

      preferences.setString("username", username);
  }

  saveEmail(String email) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();

      preferences.setString("email", email);
  }

  Future<bool> signUpCreate(AuthSignUp data) async {
    final response = await client.post(
      "$baseUrl/api/signup",
      headers: {"content-type": "application/json"},
      body: authSignUpToJson(data)
    );

    if (response.statusCode == 200) {
      return true;
    } else {
      return false;
    }
  }

  Future<bool> signIn(AuthSignIn authSignIn) async {
    final response = await client.post(
      "$baseUrl/api/signin",
      headers: {"content-type": "application/json"},
      body: authSignInToJson(authSignIn)
    );

    var result  = json.decode(response.body);
    signInAuth  = result['auth'];

    saveUsername(result['name']);
    saveEmail(result['email']);

    if(signInAuth){
      return true;
    }
    else{
      return false;
    }
  }
}