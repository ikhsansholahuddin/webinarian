part of 'shared.dart';

const double defaultMargin  = 24;

Color mainColor = Color(0xFFF73857);

TextStyle mainTextFont      = GoogleFonts.raleway().copyWith(color: Color(0xFFF73857), fontWeight: FontWeight.w500);
TextStyle blackTextFont     = GoogleFonts.raleway().copyWith(color: Colors.black, fontWeight: FontWeight.w500);
TextStyle greyTextFont      = GoogleFonts.raleway().copyWith(color: Color(0xFFA4A4A4), fontWeight: FontWeight.w500);
TextStyle whiteTextFont     = GoogleFonts.raleway().copyWith(color: Color(0xFFFFFFFF), fontWeight: FontWeight.w500);