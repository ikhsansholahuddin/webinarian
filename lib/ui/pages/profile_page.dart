part of 'pages.dart';

class ProfilePage extends StatefulWidget {
  final User user;

  ProfilePage(this.user);

  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  String username, email;

  removePref() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    prefs.remove("authSignIn");
  }

  getUsername() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    
    setState(() {
      username = prefs.getString("username");
    });
  }

  getEmail() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    
    setState(() {
      email = prefs.getString("email");
    });
  }

  @override
  void initState() {
    super.initState();
    getUsername();
    getEmail();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: Colors.white,
        padding: EdgeInsets.symmetric(horizontal: defaultMargin),
        child: ListView(
          children: <Widget>[
            SizedBox(height: 40),
            Center(
                child: Column(
                  children: <Widget>[
                    Container(
                      width: 116,
                      height: 116,
                      child: Stack(
                        children: <Widget>[
                          Container(
                              height: 116,
                              width: 116,
                              decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  image: DecorationImage(
                                      fit: BoxFit.cover,
                                      //sementara default pc
                                      image: AssetImage('assets/user_pic.png'))))
                        ],
                      ),
                    ),

                    SizedBox(height: 32,),

                    Text(
                      username ?? "",
                      style: blackTextFont.copyWith(fontSize: 18, fontWeight: FontWeight.w800),
                    ),

                    SizedBox(height: 10,),
                    Text(
                      email ?? "",
                      style: greyTextFont.copyWith(fontSize: 16),
                    ),

                    SizedBox(height: 32),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Container(
                          margin: EdgeInsets.only(right: 12),
                          width: 20,
                          height: 20,
                          child: Image.asset("assets/edit_profile_icon.png"),
                        ),
                        Text(
                          "Edit Profile",
                          style: blackTextFont.copyWith(fontSize: 16, fontWeight: FontWeight.w600)
                        )
                      ],
                    ),
                    SizedBox(height: 13,),
                    Divider(
                      color: Color(0xFFD8D8D8),
                      thickness: 2,
                    ),
                    SizedBox(height: 10),

                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Container(
                          margin: EdgeInsets.only(right: 12),
                          width: 20,
                          height: 20,
                          child: Image.asset("assets/help_icon.png"),
                        ),
                        Text(
                          "Help",
                          style: blackTextFont.copyWith(fontSize: 16, fontWeight: FontWeight.w600)
                        )
                      ],
                    ),
                    SizedBox(height: 13,),
                    Divider(
                      color: Color(0xFFD8D8D8),
                      thickness: 2,
                    ),
                    SizedBox(height: 10),

                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Container(
                          margin: EdgeInsets.only(right: 12),
                          width: 20,
                          height: 20,
                          child: Image.asset("assets/change_language_icon.png"),
                        ),
                        Text(
                          "Change Language",
                          style: blackTextFont.copyWith(fontSize: 16, fontWeight: FontWeight.w600)
                        )
                      ],
                    ),
                    SizedBox(height: 13,),
                    Divider(
                      color: Color(0xFFD8D8D8),
                      thickness: 2,
                    ),
                    SizedBox(height: 10),

                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Container(
                          margin: EdgeInsets.only(right: 12),
                          width: 20,
                          height: 20,
                          child: Image.asset("assets/rate_icon.png"),
                        ),
                        Text(
                          "Rate Webinarian",
                          style: blackTextFont.copyWith(fontSize: 16, fontWeight: FontWeight.w600)
                        )
                      ],
                    ),
                    SizedBox(height: 13,),
                    Divider(
                      color: Color(0xFFD8D8D8),
                      thickness: 2,
                    ),
                    SizedBox(height: 10),

                    RaisedButton(
                      color: mainColor,
                      onPressed: (){
                        setState(() {
                          removePref();
                        });

                        context.bloc<PageBloc>().add(GoToSignInPage());
                      },
                      child: Text(
                        "Sign Out",
                        style: whiteTextFont.copyWith(fontSize: 14, fontWeight: FontWeight.w600),
                      ),
                    ),
                    SizedBox(height: 20,)
                  ],
                ),
              ),
          ],
        ),
      ),
    );
  }
}
