part of 'pages.dart';

class SplashPage extends StatefulWidget {
  @override
  _SplashPageState createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Container(
        padding: EdgeInsets.symmetric(horizontal: defaultMargin),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Container(
              height: 133,
              decoration: BoxDecoration(
                  image: DecorationImage(image: AssetImage("assets/logo.png"))),
            ),

            Container(
              margin: EdgeInsets.only(top: 50, bottom: 17),
              child: Text(
                "WEBINARIAN",
                style: blackTextFont.copyWith(fontSize: 18, fontWeight: FontWeight.bold),
              ),
            ),

            Container(
              margin: EdgeInsets.only(bottom: 50),
              child: Text(
                "Find The Most Suitable \n Webinar For You",
                style: greyTextFont.copyWith(fontSize: 18),
                textAlign: TextAlign.center,
              ),
            ),

            Container(
              width: 250,
              height: 46,
              margin: EdgeInsets.only(bottom: 13),
              child: RaisedButton(color: mainColor,
                onPressed: (){
                  context.bloc<PageBloc>().add(GoToSignUpPage(RegistrationData()));
                },
                child: Text(
                  "Get Started",
                  style: whiteTextFont.copyWith(fontSize: 18),
                ),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(8)
                ),
              ),
            ),

            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  "Already have an account? ",
                  style: greyTextFont.copyWith(fontSize: 14),
                ),

                GestureDetector(
                  onTap: (){
                    context.bloc<PageBloc>().add(GoToSignInPage());
                  },
                  child: Text(
                    "Sign In",
                    style: mainTextFont.copyWith(fontSize: 14, fontWeight: FontWeight.w600),
                  ),
                )
              ],
            )
          ],
        ),
      ),
    );
  }
}
