part of 'pages.dart';

class MainPage extends StatefulWidget{
  @override
  MainPageState createState() => MainPageState();
}

class MainPageState extends State<MainPage>{
  int _selectedTabIndex = 1;

  @override
  Widget build(BuildContext context) {
      void _onNavBarTapped(int index){
        setState(() {
          if(index == 3){
            context.bloc<PageBloc>().add(GoToSplashPage());
          }
          else{
            _selectedTabIndex = index;
          }
        });
      }

      final _listPage = <Widget>[
        FavoritePage(),
        HomePage(),
        ProfilePage(User())
      ];

    final _bottomNavBarItems = <BottomNavigationBarItem>[
      BottomNavigationBarItem(
        icon: Icon(Icons.favorite),
        title: Text("")
      ),
      BottomNavigationBarItem(
        icon: Icon(Icons.home),
        title: Text("")
      ),
      BottomNavigationBarItem(
        icon: Icon(Icons.person),
        title: Text("")
      ),
    ];

    final _bottomNavBar = BottomNavigationBar(
      items: _bottomNavBarItems,
      currentIndex: _selectedTabIndex,
      selectedItemColor: Colors.black,
      unselectedItemColor: Colors.grey,
      onTap: _onNavBarTapped,
    );

    return Scaffold(
      body: Center(
        child: _listPage[_selectedTabIndex],
      ),
      bottomNavigationBar: _bottomNavBar,
    );
  }
}
