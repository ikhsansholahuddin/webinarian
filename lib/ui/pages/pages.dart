import 'package:flutter/material.dart';
import 'package:webinarian/bloc/blocs.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:webinarian/ui/widgets/widgets.dart';
import 'package:webinarian/models/models.dart';
import 'package:webinarian/services/services.dart';
import 'package:webinarian/shared/shared.dart';
import 'package:email_validator/email_validator.dart';
import 'package:flushbar/flushbar.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:shared_preferences/shared_preferences.dart';

part 'splash_page.dart';
part 'sign_in_page.dart';
part 'sign_up_page.dart';
part 'main_page.dart';
part 'home_page.dart';
part 'profile_page.dart';
part 'favorite_page.dart';
part 'ticket_detail_page.dart';
part 'register_page.dart';
part 'wrapper.dart';