part of 'pages.dart';

class SignUpPage extends StatefulWidget {
  final RegistrationData registrationData;

  SignUpPage(this.registrationData);

  @override
  _SignUpPageState createState() => _SignUpPageState();
}

class _SignUpPageState extends State<SignUpPage> {
  ApiService _apiService = ApiService();

  TextEditingController _controllerName = TextEditingController();
  TextEditingController _controllerEmail = TextEditingController();
  TextEditingController _controllerPassword = TextEditingController();

  bool isSignUp = false;

  @override
  void initState() {
    super.initState();

    _controllerName.text = widget.registrationData.name;
    _controllerEmail.text = widget.registrationData.email;
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        context.bloc<PageBloc>().add(GoToSplashPage());

        return;
      },
      child: Scaffold(
        body: Container(
          color: Colors.white,
          padding: EdgeInsets.symmetric(horizontal: defaultMargin),
          child: ListView(
            children: <Widget>[
              Container(
                margin: EdgeInsets.only(top: 20, bottom: 24),
                child: Stack(
                  children: <Widget>[
                    Align(
                      alignment: Alignment.centerLeft,
                      child: GestureDetector(
                        onTap: () {
                          context.bloc<PageBloc>().add(GoToSplashPage());
                        },
                        child: Icon(Icons.arrow_back, color: Colors.black),
                      ),
                    ),
                    Center(
                      child: Text("Create Your \n New Account",
                          style: blackTextFont.copyWith(
                              fontSize: 18, fontWeight: FontWeight.w800),
                          textAlign: TextAlign.center),
                    )
                  ],
                ),
              ),
              Center(
                child: Container(
                  width: 89,
                  height: 89,
                  child: Stack(
                    children: <Widget>[
                      Container(
                          height: 89,
                          width: 89,
                          decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              image: DecorationImage(
                                  fit: BoxFit.cover,
                                  //sementara default pc
                                  image: AssetImage('assets/user_pic.png'))))
                    ],
                  ),
                ),
              ),
              SizedBox(
                height: 32,
              ),
              Text("Full Name",
                  style: greyTextFont.copyWith(
                    fontSize: 14,
                  )),
              SizedBox(height: 8),
              TextField(
                style: blackTextFont.copyWith(fontSize: 14),
                controller: _controllerName,
                decoration: InputDecoration(
                    focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.black, width: 2),
                        borderRadius: BorderRadius.circular(8)),
                    enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.black, width: 1),
                        borderRadius: BorderRadius.circular(8)),
                    border: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.black, width: 1),
                        borderRadius: BorderRadius.circular(8)),
                    hintText: "Full Name",
                    hintStyle: blackTextFont.copyWith(fontSize: 14)),
              ),
              SizedBox(
                height: 15,
              ),
              Text("Email Address",
                  style: greyTextFont.copyWith(
                    fontSize: 14,
                  )),
              SizedBox(height: 8),
              TextField(
                style: blackTextFont.copyWith(fontSize: 14),
                controller: _controllerEmail,
                decoration: InputDecoration(
                    focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.black, width: 2),
                        borderRadius: BorderRadius.circular(8)),
                    enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.black, width: 1),
                        borderRadius: BorderRadius.circular(8)),
                    border: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.black, width: 1),
                        borderRadius: BorderRadius.circular(8)),
                    hintText: "Email Address",
                    hintStyle: blackTextFont.copyWith(fontSize: 14)),
              ),
              SizedBox(
                height: 15,
              ),
              Text("Password",
                  style: greyTextFont.copyWith(
                    fontSize: 14,
                  )),
              SizedBox(height: 8),
              TextField(
                style: blackTextFont.copyWith(fontSize: 14),
                controller: _controllerPassword,
                obscureText: true,
                decoration: InputDecoration(
                    focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.black, width: 2),
                        borderRadius: BorderRadius.circular(8)),
                    enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.black, width: 1),
                        borderRadius: BorderRadius.circular(8)),
                    border: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.black, width: 1),
                        borderRadius: BorderRadius.circular(8)),
                    hintText: "Password",
                    hintStyle: blackTextFont.copyWith(fontSize: 14)),
              ),
              SizedBox(
                height: 41,
              ),
              Center(
                child: isSignUp ? SpinKitFadingCircle(
                  color: mainColor
                ) : FloatingActionButton(
                  child: Icon(Icons.arrow_forward),
                  backgroundColor: mainColor,
                  elevation: 0,
                  onPressed: () async {
                    setState(() {
                      isSignUp = true;
                    });

                    if (!(_controllerName.text.trim() != "" &&
                        _controllerEmail.text.trim() != "" &&
                        _controllerPassword.text.trim() != "")) {
                      setState(() {
                        isSignUp = false;
                      });
                      Flushbar(
                        duration: Duration(milliseconds: 1500),
                        flushbarPosition: FlushbarPosition.TOP,
                        backgroundColor: Color(0xFFFF5C83),
                        message: "Please fill all the fields",
                      )..show(context);
                    } else if (_controllerPassword.text.length < 6) {
                      setState(() {
                        isSignUp = false;
                      });
                      Flushbar(
                        duration: Duration(milliseconds: 1500),
                        flushbarPosition: FlushbarPosition.TOP,
                        backgroundColor: Color(0xFFFF5C83),
                        message: "Password's length min 6 characters",
                      )..show(context);
                    } else if (!EmailValidator.validate(
                        _controllerEmail.text)) {
                      setState(() {
                        isSignUp = false;
                      });
                      Flushbar(
                        duration: Duration(milliseconds: 1500),
                        flushbarPosition: FlushbarPosition.TOP,
                        backgroundColor: Color(0xFFFF5C83),
                        message: "Wrong formatted email address",
                      )..show(context);
                    } else {

                      //ini untuk menyimpan pada model registration data terlebih dulu
                      widget.registrationData.name = _controllerName.text;
                      widget.registrationData.email = _controllerEmail.text;
                      widget.registrationData.password = _controllerPassword.text;

                      // //ini untuk menyimpan ke api
                      AuthSignUp signUp = AuthSignUp(
                        name: widget.registrationData.name, 
                        email: widget.registrationData.email, 
                        password: widget.registrationData.password
                      );

                      await _apiService.signUpCreate(signUp).then((isSuccess){
                        if(isSuccess){
                          Flushbar(
                            duration: Duration(milliseconds: 1500),
                            flushbarPosition: FlushbarPosition.TOP,
                            backgroundColor: Colors.green,
                            message: "Register successfull, please sign in",
                          )..show(context);

                          context.bloc<PageBloc>().add(GoToSignInPage());
                        }
                        else{
                          setState(() {
                            isSignUp = false;
                          });

                          Flushbar(
                            duration: Duration(milliseconds: 1500),
                            flushbarPosition: FlushbarPosition.TOP,
                            backgroundColor: Color(0xFFFF5C83),
                            message: "Failed, email has registered",
                          )..show(context);
                        }
                      });
                    }
                  },
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
