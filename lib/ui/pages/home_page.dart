part of 'pages.dart';

class HomePage extends StatefulWidget{
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage>{
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        children: <Widget>[
          Container(
            height: 220,
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage("assets/home-bg.png"),
                fit: BoxFit.cover
              )
            ),
          ),

          SizedBox(height: 14,),

          Container(
            margin: EdgeInsets.symmetric(horizontal: 16),
            child: Text("Your Webinar", 
              style: TextStyle(
                color: Color(0xFF999292),
                fontSize: 16,
                fontWeight: FontWeight.w600
              ),
            )
          ),
          
          SizedBox(height: 13,),

          Container(
            height: 150,
            child: ListView(
              scrollDirection: Axis.horizontal,
              children: <Widget>[
                //webinar card
                Container(
                  margin: EdgeInsets.symmetric(horizontal: 8),
                  width: MediaQuery.of(context).size.width * 0.325,
                  height: 150,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: Color(0xFFF6F1F1)
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.symmetric(horizontal: 4),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            SizedBox(height: 8,),

                            Container(
                              width: 50,
                              height: 50,
                              decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                image: DecorationImage(
                                  image: AssetImage("assets/webinar-person.png"),
                                  fit: BoxFit.cover
                                )
                              ),
                            ),

                            SizedBox(height: 8,),

                            Text("Flutter Expert",
                              style: TextStyle(
                                color: Color(0xFF000000),
                                fontSize: 12,
                                fontWeight: FontWeight.bold
                              ),
                            ),

                            SizedBox(height: 9,),

                            Text("10 Oktober 2020", 
                              style: TextStyle(
                                color: Color(0xFFEB3636),
                                fontSize: 12,
                                fontWeight: FontWeight.w400
                              ),
                            ),

                            SizedBox(height: 4,),

                            Row(
                              children: <Widget>[
                                Icon(Icons.timer, size: 16,),
                                Text("10:00 - 12:00", 
                                  style: TextStyle(
                                    color: Color(0xFF999292),
                                    fontSize: 12,
                                    fontWeight: FontWeight.w400
                                  ),
                                ),
                              ],
                            ),

                            SizedBox(height: 4,),

                            Row(
                              children: <Widget>[
                                Icon(Icons.label_outline, size: 16,),
                                Text("Rp. 100.000", 
                                  style: TextStyle(
                                    color: Color(0xFF999292),
                                    fontSize: 12,
                                    fontWeight: FontWeight.w400
                                  ),
                                ),
                              ],
                            )
                          ],
                        )
                      ),
                    ],
                  ),
                ),

                Container(
                  margin: EdgeInsets.symmetric(horizontal: 8),
                  width: MediaQuery.of(context).size.width * 0.325,
                  height: 150,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: Color(0xFFF6F1F1)
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.symmetric(horizontal: 4),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            SizedBox(height: 8,),

                            Container(
                              width: 50,
                              height: 50,
                              decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                image: DecorationImage(
                                  image: AssetImage("assets/webinar-coins.png"),
                                  fit: BoxFit.cover
                                )
                              ),
                            ),

                            SizedBox(height: 8,),

                            Text("Block Chain",
                              style: TextStyle(
                                color: Color(0xFF000000),
                                fontSize: 12,
                                fontWeight: FontWeight.bold
                              ),
                            ),

                            SizedBox(height: 9,),

                            Text("10 Oktober 2020", 
                              style: TextStyle(
                                color: Color(0xFFEB3636),
                                fontSize: 12,
                                fontWeight: FontWeight.w400
                              ),
                            ),

                            SizedBox(height: 4,),

                            Row(
                              children: <Widget>[
                                Icon(Icons.timer, size: 16,),
                                Text("10:00 - 12:00", 
                                  style: TextStyle(
                                    color: Color(0xFF999292),
                                    fontSize: 12,
                                    fontWeight: FontWeight.w400
                                  ),
                                ),
                              ],
                            ),

                            SizedBox(height: 4,),

                            Row(
                              children: <Widget>[
                                Icon(Icons.label_outline, size: 16,),
                                Text("Rp. 100.000", 
                                  style: TextStyle(
                                    color: Color(0xFF999292),
                                    fontSize: 12,
                                    fontWeight: FontWeight.w400
                                  ),
                                ),
                              ],
                            )
                          ],
                        )
                      ),
                    ],
                  ),
                ),

                Container(
                  margin: EdgeInsets.symmetric(horizontal: 8),
                  width: MediaQuery.of(context).size.width * 0.325,
                  height: 150,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: Color(0xFFF6F1F1)
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.symmetric(horizontal: 4),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            SizedBox(height: 8,),

                            Container(
                              width: 50,
                              height: 50,
                              decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                image: DecorationImage(
                                  image: AssetImage("assets/webinar-person.png"),
                                  fit: BoxFit.cover
                                )
                              ),
                            ),

                            SizedBox(height: 8,),

                            Text("Flutter Expert",
                              style: TextStyle(
                                color: Color(0xFF000000),
                                fontSize: 12,
                                fontWeight: FontWeight.bold
                              ),
                            ),

                            SizedBox(height: 9,),

                            Text("10 Oktober 2020", 
                              style: TextStyle(
                                color: Color(0xFFEB3636),
                                fontSize: 12,
                                fontWeight: FontWeight.w400
                              ),
                            ),

                            SizedBox(height: 4,),

                            Row(
                              children: <Widget>[
                                Icon(Icons.timer, size: 16,),
                                Text("10:00 - 12:00", 
                                  style: TextStyle(
                                    color: Color(0xFF999292),
                                    fontSize: 12,
                                    fontWeight: FontWeight.w400
                                  ),
                                ),
                              ],
                            ),

                            SizedBox(height: 4,),

                            Row(
                              children: <Widget>[
                                Icon(Icons.label_outline, size: 16,),
                                Text("Rp. 100.000", 
                                  style: TextStyle(
                                    color: Color(0xFF999292),
                                    fontSize: 12,
                                    fontWeight: FontWeight.w400
                                  ),
                                ),
                              ],
                            )
                          ],
                        )
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),

          SizedBox(height: 32,),

          //webinar card
          GestureDetector(
            onTap: (){
              context.bloc<PageBloc>().add(GoToTicketDetailPage());
            },
            child: Container(
              margin: EdgeInsets.symmetric(horizontal: 8),
              child: Column(
                children: <Widget>[
                  Container(
                    height: 220,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(14),
                      border: Border.all(
                        color: Color(0xFFA0A4A8),
                        width: 1
                      ),
                    ),
                    child: Column(
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            Container(
                              margin: EdgeInsets.only(left: 12, top: 20),
                              child: Row(
                                children: <Widget>[
                                  Container(
                                    margin: EdgeInsets.only(right: 26),
                                    width: 50,
                                    height: 50,
                                    decoration: BoxDecoration(
                                      shape: BoxShape.circle,
                                      image: DecorationImage(
                                        image: AssetImage("assets/webinar-coins.png"),
                                        fit: BoxFit.cover
                                      )
                                    ),
                                  ),

                                  Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Text("Block Chain",
                                        style: TextStyle(
                                          color: Color(0xFF000000),
                                          fontSize: 16,
                                          fontWeight: FontWeight.bold
                                        ),
                                      ),

                                      SizedBox(height: 6,),

                                      Row(
                                        children: <Widget>[
                                          Icon(Icons.person_outline, size: 20,),
                                          Text("Gary Vaynerchuk", 
                                            style: TextStyle(
                                              color: Color(0xFF999292),
                                              fontSize: 12,
                                              fontWeight: FontWeight.w400
                                            ),
                                          ),
                                        ],
                                      ),

                                      SizedBox(height: 4,),

                                      Row(
                                        children: <Widget>[
                                          Icon(Icons.label_outline, size: 20,),
                                          Text("Rp. 100.000", 
                                            style: TextStyle(
                                              color: Color(0xFF999292),
                                              fontSize: 12,
                                              fontWeight: FontWeight.w400
                                            ),
                                          ),
                                        ],
                                      ),

                                      SizedBox(height: 4,),

                                      Row(
                                        children: <Widget>[
                                          Icon(Icons.calendar_today, size: 16,),
                                          Text("10 Oktober 2020", 
                                            style: TextStyle(
                                              color: Color(0xFF999292),
                                              fontSize: 12,
                                              fontWeight: FontWeight.w400
                                            ),
                                          ),
                                        ],
                                      ),

                                      SizedBox(height: 4,),

                                      Text("Registered",
                                        style: TextStyle(
                                          color: Color(0xFF19930F),
                                          fontSize: 14,
                                          fontWeight: FontWeight.w800
                                        ),
                                      ),
                                    ],
                                  )
                                ],
                              ),
                            ),
                          ],
                        ),

                        SizedBox(height: 8,),

                        //Paragraf
                        Container(
                          margin: EdgeInsets.symmetric(horizontal: 12),
                          child: Text('Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Tincidunt vitae semper quis lectus nulla at volutpat diam.',
                            maxLines: 4,
                            overflow: TextOverflow.ellipsis,
                          ),
                        )
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),

          SizedBox(height: 15),

          GestureDetector(
            onTap: (){
              context.bloc<PageBloc>().add(GoToTicketDetailPage());
            },
            child: Container(
              margin: EdgeInsets.symmetric(horizontal: 8),
              child: Column(
                children: <Widget>[
                  Container(
                    height: 220,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(14),
                      border: Border.all(
                        color: Color(0xFFA0A4A8),
                        width: 1
                      ),
                    ),
                    child: Column(
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            Container(
                              margin: EdgeInsets.only(left: 12, top: 20),
                              child: Row(
                                children: <Widget>[
                                  Container(
                                    margin: EdgeInsets.only(right: 26),
                                    width: 50,
                                    height: 50,
                                    decoration: BoxDecoration(
                                      shape: BoxShape.circle,
                                      image: DecorationImage(
                                        image: AssetImage("assets/webinar-coins.png"),
                                        fit: BoxFit.cover
                                      )
                                    ),
                                  ),

                                  Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Text("Block Chain",
                                        style: TextStyle(
                                          color: Color(0xFF000000),
                                          fontSize: 16,
                                          fontWeight: FontWeight.bold
                                        ),
                                      ),

                                      SizedBox(height: 6,),

                                      Row(
                                        children: <Widget>[
                                          Icon(Icons.person_outline, size: 20,),
                                          Text("Gary Vaynerchuk", 
                                            style: TextStyle(
                                              color: Color(0xFF999292),
                                              fontSize: 12,
                                              fontWeight: FontWeight.w400
                                            ),
                                          ),
                                        ],
                                      ),

                                      SizedBox(height: 4,),

                                      Row(
                                        children: <Widget>[
                                          Icon(Icons.label_outline, size: 20,),
                                          Text("Rp. 100.000", 
                                            style: TextStyle(
                                              color: Color(0xFF999292),
                                              fontSize: 12,
                                              fontWeight: FontWeight.w400
                                            ),
                                          ),
                                        ],
                                      ),

                                      SizedBox(height: 4,),

                                      Row(
                                        children: <Widget>[
                                          Icon(Icons.calendar_today, size: 16,),
                                          Text("10 Oktober 2020", 
                                            style: TextStyle(
                                              color: Color(0xFF999292),
                                              fontSize: 12,
                                              fontWeight: FontWeight.w400
                                            ),
                                          ),
                                        ],
                                      ),

                                      SizedBox(height: 4,),

                                      Text("Registered",
                                        style: TextStyle(
                                          color: Color(0xFF19930F),
                                          fontSize: 14,
                                          fontWeight: FontWeight.w800
                                        ),
                                      ),
                                    ],
                                  )
                                ],
                              ),
                            ),
                          ],
                        ),

                        SizedBox(height: 8,),

                        //Paragraf
                        Container(
                          margin: EdgeInsets.symmetric(horizontal: 12),
                          child: Text('Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Tincidunt vitae semper quis lectus nulla at volutpat diam.',
                            maxLines: 4,
                            overflow: TextOverflow.ellipsis,
                          ),
                        )
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),

          SizedBox(height: 15),

          GestureDetector(
            onTap: (){
              context.bloc<PageBloc>().add(GoToTicketDetailPage());
            },
            child: Container(
              margin: EdgeInsets.symmetric(horizontal: 8),
              child: Column(
                children: <Widget>[
                  Container(
                    height: 220,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(14),
                      border: Border.all(
                        color: Color(0xFFA0A4A8),
                        width: 1
                      ),
                    ),
                    child: Column(
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            Container(
                              margin: EdgeInsets.only(left: 12, top: 20),
                              child: Row(
                                children: <Widget>[
                                  Container(
                                    margin: EdgeInsets.only(right: 26),
                                    width: 50,
                                    height: 50,
                                    decoration: BoxDecoration(
                                      shape: BoxShape.circle,
                                      image: DecorationImage(
                                        image: AssetImage("assets/webinar-coins.png"),
                                        fit: BoxFit.cover
                                      )
                                    ),
                                  ),

                                  Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Text("Block Chain",
                                        style: TextStyle(
                                          color: Color(0xFF000000),
                                          fontSize: 16,
                                          fontWeight: FontWeight.bold
                                        ),
                                      ),

                                      SizedBox(height: 6,),

                                      Row(
                                        children: <Widget>[
                                          Icon(Icons.person_outline, size: 20,),
                                          Text("Gary Vaynerchuk", 
                                            style: TextStyle(
                                              color: Color(0xFF999292),
                                              fontSize: 12,
                                              fontWeight: FontWeight.w400
                                            ),
                                          ),
                                        ],
                                      ),

                                      SizedBox(height: 4,),

                                      Row(
                                        children: <Widget>[
                                          Icon(Icons.label_outline, size: 20,),
                                          Text("Rp. 100.000", 
                                            style: TextStyle(
                                              color: Color(0xFF999292),
                                              fontSize: 12,
                                              fontWeight: FontWeight.w400
                                            ),
                                          ),
                                        ],
                                      ),

                                      SizedBox(height: 4,),

                                      Row(
                                        children: <Widget>[
                                          Icon(Icons.calendar_today, size: 16,),
                                          Text("10 Oktober 2020", 
                                            style: TextStyle(
                                              color: Color(0xFF999292),
                                              fontSize: 12,
                                              fontWeight: FontWeight.w400
                                            ),
                                          ),
                                        ],
                                      ),

                                      SizedBox(height: 4,),

                                      Text("Registered",
                                        style: TextStyle(
                                          color: Color(0xFF19930F),
                                          fontSize: 14,
                                          fontWeight: FontWeight.w800
                                        ),
                                      ),
                                    ],
                                  )
                                ],
                              ),
                            ),
                          ],
                        ),

                        SizedBox(height: 8,),

                        //Paragraf
                        Container(
                          margin: EdgeInsets.symmetric(horizontal: 12),
                          child: Text('Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Tincidunt vitae semper quis lectus nulla at volutpat diam.',
                            maxLines: 4,
                            overflow: TextOverflow.ellipsis,
                          ),
                        )
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
