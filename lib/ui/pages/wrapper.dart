part of 'pages.dart';

class Wrapper extends StatefulWidget{
  _WrapperState createState() => _WrapperState();
}

class _WrapperState extends State<Wrapper> {
  bool _login = false;
  var value;
  PageEvent prevPageEvent;
  
  getPref() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();

    setState(() {
      value = preferences.getBool("authSignIn");

      _login = value;
    });
  }

  @override
  void initState() {
    super.initState();
    getPref();
  }

  @override
  Widget build(BuildContext context) {
    if(_login){
      if(!(prevPageEvent is GoToMainPage)){
        prevPageEvent = GoToMainPage();
        context.bloc<PageBloc>().add(GoToMainPage());
      }
    }
    else{
      if(!(prevPageEvent is GoToSplashPage)){
        prevPageEvent = GoToSplashPage();

        context.bloc<PageBloc>().add(GoToSplashPage());
      }
    }

    return BlocBuilder<PageBloc, PageState>(
      builder: (_, pageState) => (pageState is OnSplashPage) 
        ? SplashPage()
        : (pageState is OnSignInPage)
          ? SignInPage()
          : (pageState is OnSignUpPage)
            ? SignUpPage(pageState.registrationData)
            : (pageState is OnTicketDetailPage)
              ? TicketDetailPage()
              : (pageState is OnRegisterPage)
                ? RegisterPage()
                : MainPage()
    );
  }
}
