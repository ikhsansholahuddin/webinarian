part of 'pages.dart';

class SignInPage extends StatefulWidget {
  @override
  _SignInPageState createState() => _SignInPageState();
}

class _SignInPageState extends State<SignInPage> {
  ApiService _apiService = ApiService();
  
  TextEditingController _controllerEmail = TextEditingController();
  TextEditingController _controllerPassword = TextEditingController();

  bool isSignIn = false;

  savePref(bool auth) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();

    setState(() {
      preferences.setBool("authSignIn", auth);
    });
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        context.bloc<PageBloc>().add(GoToSplashPage());
      
        return;
      },
      child: Scaffold(
        backgroundColor: Colors.white,
        body: Container(
          padding: EdgeInsets.symmetric(horizontal: defaultMargin),
          child: ListView(
            children: <Widget>[
              SizedBox(
                height: 30,
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  SizedBox(
                    height: 70,
                    child: Image.asset("assets/logo.png"),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 15, bottom: 83),
                    child: Text(
                      "Please Sign In",
                      style: blackTextFont.copyWith(fontSize: 18, fontWeight: FontWeight.w800)
                    ),
                  ),

                  Text(
                    "Email Address",
                    style: greyTextFont.copyWith(fontSize: 14,)
                  ),

                  SizedBox(height: 8),
                  TextField(
                    style: blackTextFont.copyWith(fontSize: 14),
                    controller: _controllerEmail,
                    decoration: InputDecoration(
                      focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.black, width: 2),
                        borderRadius: BorderRadius.circular(8)
                      ),
                      enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.black, width: 1),
                        borderRadius: BorderRadius.circular(8)
                      ),
                      border: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.black, width: 1),
                        borderRadius: BorderRadius.circular(8)
                      ),
                      hintText: "Email Address",
                      hintStyle: blackTextFont.copyWith(fontSize: 14)
                    ),
                  ),

                  SizedBox(height: 15,),
                  Text(
                    "Password",
                    style: greyTextFont.copyWith(fontSize: 14,)
                  ),

                  SizedBox(height: 8),
                  TextField(
                    obscureText: true,
                    style: blackTextFont.copyWith(fontSize: 14),
                    controller: _controllerPassword,
                    decoration: InputDecoration(
                      focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.black, width: 2),
                        borderRadius: BorderRadius.circular(8)
                      ),
                      enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.black, width: 1),
                        borderRadius: BorderRadius.circular(8)
                      ),
                      border: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.black, width: 1),
                        borderRadius: BorderRadius.circular(8)
                      ),
                      hintText: "Password",
                      hintStyle: blackTextFont.copyWith(fontSize: 14)
                    ),
                  ),

                  SizedBox(height: 43,),
                  Center(
                    child: isSignIn ? SpinKitFadingCircle(
                      color: mainColor
                    ) : FloatingActionButton(
                      child: Icon(Icons.arrow_forward),
                      backgroundColor: mainColor,
                      elevation: 0,
                      onPressed: () async {
                        setState(() {
                          isSignIn = true;
                        });

                        AuthSignIn signIn = AuthSignIn(
                          email: _controllerEmail.text,
                          password: _controllerPassword.text
                        );

                        if(!(_controllerEmail.text.trim() != "" && _controllerPassword.text.trim() != "")){
                          setState(() {
                            isSignIn = false;
                          });
                          Flushbar(
                            duration: Duration(milliseconds: 1500),
                            flushbarPosition: FlushbarPosition.TOP,
                            backgroundColor: Color(0xFFFF5C83),
                            message: "Please fill all the fields",
                          )..show(context);
                        }
                        else{
                          await _apiService.signIn(signIn).then((isTrue){
                            if(isTrue){
                              setState(() {
                                savePref(true);
                              });

                              context.bloc<PageBloc>().add(GoToMainPage());
                            }
                            else{
                              setState(() {
                                isSignIn = false;
                              });

                              Flushbar(
                                duration: Duration(milliseconds: 2500),
                                flushbarPosition: FlushbarPosition.TOP,
                                backgroundColor: Color(0xFFFF5C83),
                                message: "Authentication failed, please enter valid email and password",
                              )..show(context);
                            }
                          });
                        }
                      },
                    ),
                  ),
                  
                  SizedBox(height: 42),

                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text(
                        "Do not have an account? ",
                        style: greyTextFont.copyWith(fontSize: 14),
                      ),

                      GestureDetector(
                        onTap: (){
                          context.bloc<PageBloc>().add(GoToSignUpPage(RegistrationData()));
                        },
                        child: Text(
                          "Sign Up",
                          style: mainTextFont.copyWith(fontSize: 14, fontWeight: FontWeight.w600),
                        ),
                      )
                    ],
                  )
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
