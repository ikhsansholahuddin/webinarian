part of 'pages.dart';

class TicketDetailPage extends StatefulWidget {
  _TicketDetailPageState createState() => _TicketDetailPageState();
}

class _TicketDetailPageState extends State<TicketDetailPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        children: <Widget>[
          SizedBox(
            height: 16,
          ),
          Container(
            margin: EdgeInsets.symmetric(horizontal: 16),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  child: GestureDetector(
                      onTap: () {
                        context.bloc<PageBloc>().add(GoToMainPage());
                      },
                      child: Icon(Icons.arrow_back, color: Colors.black)),
                ),
                Expanded(
                  child: Center(
                    child: Text(
                      "TICKET DETAILS",
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: 26,
                          fontWeight: FontWeight.w600),
                    ),
                  ),
                ),
              ],
            ),
          ),
          Divider(
            thickness: 1,
            color: Color(0xFF999292),
          ),
          SizedBox(
            height: 25,
          ),
          Container(
            margin: EdgeInsets.symmetric(horizontal: 27),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  height: 220,
                  decoration: BoxDecoration(
                      image: DecorationImage(
                          image: AssetImage("assets/detail-bg.png"),
                          fit: BoxFit.cover)),
                ),
                SizedBox(
                  height: 38,
                ),
                Text(
                  "Block Chain",
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: 24,
                      fontWeight: FontWeight.w600),
                ),

                SizedBox(
                  height: 40,
                ),
                Text(
                  "Saturday, 10 October 2020",
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: 18,
                      fontWeight: FontWeight.w600),
                ),
                SizedBox(
                  height: 14,
                ),
                Text(
                  "09:00 - 12:00",
                  style: TextStyle(
                      color: Color(0xFF50555C),
                      fontSize: 16,
                      fontWeight: FontWeight.w600),
                ),

                SizedBox(
                  height: 14,
                ),
                Text(
                  "Contact Information",
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: 18,
                      fontWeight: FontWeight.w600),
                ),
                SizedBox(
                  height: 14,
                ),
                Text(
                  "blockchain@information.com",
                  style: TextStyle(
                      color: Color(0xFF50555C),
                      fontSize: 16,
                      fontWeight: FontWeight.w600),
                ),
                SizedBox(
                  height: 14,
                ),
                Text(
                  "Ticket",
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: 18,
                      fontWeight: FontWeight.w600),
                ),
                SizedBox(
                  height: 14,
                ),
                Text(
                  "2 Tickets",
                  style: TextStyle(
                      color: Color(0xFF50555C),
                      fontSize: 16,
                      fontWeight: FontWeight.w600),
                ),
                SizedBox(
                  height: 14,
                ),
                Text(
                  "Status",
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: 18,
                      fontWeight: FontWeight.w600),
                ),
                SizedBox(
                  height: 14,
                ),
                Text(
                  "Paid",
                  style: TextStyle(
                      color: Color(0xFF50555C),
                      fontSize: 16,
                      fontWeight: FontWeight.w600),
                ),

                SizedBox(
                  height: 30,
                ),

                //Button
                Row(
                  children: <Widget>[
                    Expanded(
                      child: Container(
                        height: 52,
                        child: RaisedButton(
                          color: Colors.black,
                          child: Text(
                            "REGISTER",
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 14,
                                fontWeight: FontWeight.w800),
                          ),
                          onPressed: () {
                            context.bloc<PageBloc>().add(GoToRegisterPage());
                          },
                        ),
                      ),
                    ),
                  ],
                ),

                SizedBox(
                  height: 30,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
