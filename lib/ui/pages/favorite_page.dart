part of 'pages.dart';

class FavoritePage extends StatefulWidget {
  @override
  _FavoritePageState createState() => _FavoritePageState();
}

class _FavoritePageState extends State<FavoritePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        children: <Widget>[
          SizedBox(height: 16,),

          Center(
            child: Wrap(
              children: <Widget>[
                //favorite card
                Container(
                  margin: EdgeInsets.all(8),
                  width: MediaQuery.of(context).size.width * 0.425,
                  height: 200,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: Color(0xFFF6F1F1)
                    ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.symmetric(horizontal: 12),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            SizedBox(height: 8,),

                            Container(
                              width: 50,
                              height: 50,
                              decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                image: DecorationImage(
                                  image: AssetImage("assets/webinar-person.png"),
                                  fit: BoxFit.cover
                                )
                              ),
                            ),

                            SizedBox(height: 8,),

                            Text("Flutter Expert",
                              style: TextStyle(
                                color: Color(0xFF000000),
                                fontSize: 12,
                                fontWeight: FontWeight.bold
                              ),
                            ),

                            SizedBox(height: 9,),

                            Text("10 Oktober 2020", 
                              style: TextStyle(
                                color: Color(0xFFEB3636),
                                fontSize: 12,
                                fontWeight: FontWeight.w400
                              ),
                            ),

                            SizedBox(height: 4,),

                            Row(
                              children: <Widget>[
                                Icon(Icons.timer, size: 16,),
                                Text("10:00 - 12:00", 
                                  style: TextStyle(
                                    color: Color(0xFF999292),
                                    fontSize: 12,
                                    fontWeight: FontWeight.w400
                                  ),
                                ),
                              ],
                            ),

                            SizedBox(height: 4,),

                            Row(
                              children: <Widget>[
                                Icon(Icons.label_outline, size: 16,),
                                Text("Rp. 100.000", 
                                  style: TextStyle(
                                    color: Color(0xFF999292),
                                    fontSize: 12,
                                    fontWeight: FontWeight.w400
                                  ),
                                ),
                              ],
                            ),

                            SizedBox(height: 12,),

                            Text("Register",
                              style: TextStyle(
                                color: Color(0xFFE92E2E),
                                fontSize: 14,
                                fontWeight: FontWeight.bold
                              ),
                            )
                          ],
                        )
                      ),
                    ],
                  ),
                ),

                Container(
                  margin: EdgeInsets.all(8),
                  width: MediaQuery.of(context).size.width * 0.425,
                  height: 200,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: Color(0xFFF6F1F1)
                    ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.symmetric(horizontal: 12),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            SizedBox(height: 8,),

                            Container(
                              width: 50,
                              height: 50,
                              decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                image: DecorationImage(
                                  image: AssetImage("assets/webinar-person.png"),
                                  fit: BoxFit.cover
                                )
                              ),
                            ),

                            SizedBox(height: 8,),

                            Text("Flutter Expert",
                              style: TextStyle(
                                color: Color(0xFF000000),
                                fontSize: 12,
                                fontWeight: FontWeight.bold
                              ),
                            ),

                            SizedBox(height: 9,),

                            Text("10 Oktober 2020", 
                              style: TextStyle(
                                color: Color(0xFFEB3636),
                                fontSize: 12,
                                fontWeight: FontWeight.w400
                              ),
                            ),

                            SizedBox(height: 4,),

                            Row(
                              children: <Widget>[
                                Icon(Icons.timer, size: 16,),
                                Text("10:00 - 12:00", 
                                  style: TextStyle(
                                    color: Color(0xFF999292),
                                    fontSize: 12,
                                    fontWeight: FontWeight.w400
                                  ),
                                ),
                              ],
                            ),

                            SizedBox(height: 4,),

                            Row(
                              children: <Widget>[
                                Icon(Icons.label_outline, size: 16,),
                                Text("Rp. 100.000", 
                                  style: TextStyle(
                                    color: Color(0xFF999292),
                                    fontSize: 12,
                                    fontWeight: FontWeight.w400
                                  ),
                                ),
                              ],
                            ),

                            SizedBox(height: 12,),

                            Text("Register",
                              style: TextStyle(
                                color: Color(0xFFE92E2E),
                                fontSize: 14,
                                fontWeight: FontWeight.bold
                              ),
                            )
                          ],
                        )
                      ),
                    ],
                  ),
                ),

                Container(
                  margin: EdgeInsets.all(8),
                  width: MediaQuery.of(context).size.width * 0.425,
                  height: 200,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: Color(0xFFF6F1F1)
                    ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.symmetric(horizontal: 12),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            SizedBox(height: 8,),

                            Container(
                              width: 50,
                              height: 50,
                              decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                image: DecorationImage(
                                  image: AssetImage("assets/webinar-person.png"),
                                  fit: BoxFit.cover
                                )
                              ),
                            ),

                            SizedBox(height: 8,),

                            Text("Flutter Expert",
                              style: TextStyle(
                                color: Color(0xFF000000),
                                fontSize: 12,
                                fontWeight: FontWeight.bold
                              ),
                            ),

                            SizedBox(height: 9,),

                            Text("10 Oktober 2020", 
                              style: TextStyle(
                                color: Color(0xFFEB3636),
                                fontSize: 12,
                                fontWeight: FontWeight.w400
                              ),
                            ),

                            SizedBox(height: 4,),

                            Row(
                              children: <Widget>[
                                Icon(Icons.timer, size: 16,),
                                Text("10:00 - 12:00", 
                                  style: TextStyle(
                                    color: Color(0xFF999292),
                                    fontSize: 12,
                                    fontWeight: FontWeight.w400
                                  ),
                                ),
                              ],
                            ),

                            SizedBox(height: 4,),

                            Row(
                              children: <Widget>[
                                Icon(Icons.label_outline, size: 16,),
                                Text("Rp. 100.000", 
                                  style: TextStyle(
                                    color: Color(0xFF999292),
                                    fontSize: 12,
                                    fontWeight: FontWeight.w400
                                  ),
                                ),
                              ],
                            ),

                            SizedBox(height: 12,),

                            Text("Register",
                              style: TextStyle(
                                color: Color(0xFFE92E2E),
                                fontSize: 14,
                                fontWeight: FontWeight.bold
                              ),
                            )
                          ],
                        )
                      ),
                    ],
                  ),
                ),

                Container(
                  margin: EdgeInsets.all(8),
                  width: MediaQuery.of(context).size.width * 0.425,
                  height: 200,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: Color(0xFFF6F1F1)
                    ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.symmetric(horizontal: 12),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            SizedBox(height: 8,),

                            Container(
                              width: 50,
                              height: 50,
                              decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                image: DecorationImage(
                                  image: AssetImage("assets/webinar-person.png"),
                                  fit: BoxFit.cover
                                )
                              ),
                            ),

                            SizedBox(height: 8,),

                            Text("Flutter Expert",
                              style: TextStyle(
                                color: Color(0xFF000000),
                                fontSize: 12,
                                fontWeight: FontWeight.bold
                              ),
                            ),

                            SizedBox(height: 9,),

                            Text("10 Oktober 2020", 
                              style: TextStyle(
                                color: Color(0xFFEB3636),
                                fontSize: 12,
                                fontWeight: FontWeight.w400
                              ),
                            ),

                            SizedBox(height: 4,),

                            Row(
                              children: <Widget>[
                                Icon(Icons.timer, size: 16,),
                                Text("10:00 - 12:00", 
                                  style: TextStyle(
                                    color: Color(0xFF999292),
                                    fontSize: 12,
                                    fontWeight: FontWeight.w400
                                  ),
                                ),
                              ],
                            ),

                            SizedBox(height: 4,),

                            Row(
                              children: <Widget>[
                                Icon(Icons.label_outline, size: 16,),
                                Text("Rp. 100.000", 
                                  style: TextStyle(
                                    color: Color(0xFF999292),
                                    fontSize: 12,
                                    fontWeight: FontWeight.w400
                                  ),
                                ),
                              ],
                            ),

                            SizedBox(height: 12,),

                            Text("Register",
                              style: TextStyle(
                                color: Color(0xFFE92E2E),
                                fontSize: 14,
                                fontWeight: FontWeight.bold
                              ),
                            )
                          ],
                        )
                      ),
                    ],
                  ),
                ),

                Container(
                  margin: EdgeInsets.all(8),
                  width: MediaQuery.of(context).size.width * 0.425,
                  height: 200,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: Color(0xFFF6F1F1)
                    ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.symmetric(horizontal: 12),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            SizedBox(height: 8,),

                            Container(
                              width: 50,
                              height: 50,
                              decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                image: DecorationImage(
                                  image: AssetImage("assets/webinar-person.png"),
                                  fit: BoxFit.cover
                                )
                              ),
                            ),

                            SizedBox(height: 8,),

                            Text("Flutter Expert",
                              style: TextStyle(
                                color: Color(0xFF000000),
                                fontSize: 12,
                                fontWeight: FontWeight.bold
                              ),
                            ),

                            SizedBox(height: 9,),

                            Text("10 Oktober 2020", 
                              style: TextStyle(
                                color: Color(0xFFEB3636),
                                fontSize: 12,
                                fontWeight: FontWeight.w400
                              ),
                            ),

                            SizedBox(height: 4,),

                            Row(
                              children: <Widget>[
                                Icon(Icons.timer, size: 16,),
                                Text("10:00 - 12:00", 
                                  style: TextStyle(
                                    color: Color(0xFF999292),
                                    fontSize: 12,
                                    fontWeight: FontWeight.w400
                                  ),
                                ),
                              ],
                            ),

                            SizedBox(height: 4,),

                            Row(
                              children: <Widget>[
                                Icon(Icons.label_outline, size: 16,),
                                Text("Rp. 100.000", 
                                  style: TextStyle(
                                    color: Color(0xFF999292),
                                    fontSize: 12,
                                    fontWeight: FontWeight.w400
                                  ),
                                ),
                              ],
                            ),

                            SizedBox(height: 12,),

                            Text("Register",
                              style: TextStyle(
                                color: Color(0xFFE92E2E),
                                fontSize: 14,
                                fontWeight: FontWeight.bold
                              ),
                            )
                          ],
                        )
                      ),
                    ],
                  ),
                ),

                Container(
                  margin: EdgeInsets.all(8),
                  width: MediaQuery.of(context).size.width * 0.425,
                  height: 200,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: Color(0xFFF6F1F1)
                    ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.symmetric(horizontal: 12),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            SizedBox(height: 8,),

                            Container(
                              width: 50,
                              height: 50,
                              decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                image: DecorationImage(
                                  image: AssetImage("assets/webinar-person.png"),
                                  fit: BoxFit.cover
                                )
                              ),
                            ),

                            SizedBox(height: 8,),

                            Text("Flutter Expert",
                              style: TextStyle(
                                color: Color(0xFF000000),
                                fontSize: 12,
                                fontWeight: FontWeight.bold
                              ),
                            ),

                            SizedBox(height: 9,),

                            Text("10 Oktober 2020", 
                              style: TextStyle(
                                color: Color(0xFFEB3636),
                                fontSize: 12,
                                fontWeight: FontWeight.w400
                              ),
                            ),

                            SizedBox(height: 4,),

                            Row(
                              children: <Widget>[
                                Icon(Icons.timer, size: 16,),
                                Text("10:00 - 12:00", 
                                  style: TextStyle(
                                    color: Color(0xFF999292),
                                    fontSize: 12,
                                    fontWeight: FontWeight.w400
                                  ),
                                ),
                              ],
                            ),

                            SizedBox(height: 4,),

                            Row(
                              children: <Widget>[
                                Icon(Icons.label_outline, size: 16,),
                                Text("Rp. 100.000", 
                                  style: TextStyle(
                                    color: Color(0xFF999292),
                                    fontSize: 12,
                                    fontWeight: FontWeight.w400
                                  ),
                                ),
                              ],
                            ),

                            SizedBox(height: 12,),

                            Text("Register",
                              style: TextStyle(
                                color: Color(0xFFE92E2E),
                                fontSize: 14,
                                fontWeight: FontWeight.bold
                              ),
                            )
                          ],
                        )
                      ),
                    ],
                  ),
                ),

                Container(
                  margin: EdgeInsets.all(8),
                  width: MediaQuery.of(context).size.width * 0.425,
                  height: 200,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: Color(0xFFF6F1F1)
                    ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.symmetric(horizontal: 12),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            SizedBox(height: 8,),

                            Container(
                              width: 50,
                              height: 50,
                              decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                image: DecorationImage(
                                  image: AssetImage("assets/webinar-person.png"),
                                  fit: BoxFit.cover
                                )
                              ),
                            ),

                            SizedBox(height: 8,),

                            Text("Flutter Expert",
                              style: TextStyle(
                                color: Color(0xFF000000),
                                fontSize: 12,
                                fontWeight: FontWeight.bold
                              ),
                            ),

                            SizedBox(height: 9,),

                            Text("10 Oktober 2020", 
                              style: TextStyle(
                                color: Color(0xFFEB3636),
                                fontSize: 12,
                                fontWeight: FontWeight.w400
                              ),
                            ),

                            SizedBox(height: 4,),

                            Row(
                              children: <Widget>[
                                Icon(Icons.timer, size: 16,),
                                Text("10:00 - 12:00", 
                                  style: TextStyle(
                                    color: Color(0xFF999292),
                                    fontSize: 12,
                                    fontWeight: FontWeight.w400
                                  ),
                                ),
                              ],
                            ),

                            SizedBox(height: 4,),

                            Row(
                              children: <Widget>[
                                Icon(Icons.label_outline, size: 16,),
                                Text("Rp. 100.000", 
                                  style: TextStyle(
                                    color: Color(0xFF999292),
                                    fontSize: 12,
                                    fontWeight: FontWeight.w400
                                  ),
                                ),
                              ],
                            ),

                            SizedBox(height: 12,),

                            Text("Register",
                              style: TextStyle(
                                color: Color(0xFFE92E2E),
                                fontSize: 14,
                                fontWeight: FontWeight.bold
                              ),
                            )
                          ],
                        )
                      ),
                    ],
                  ),
                ),

                Container(
                  margin: EdgeInsets.all(8),
                  width: MediaQuery.of(context).size.width * 0.425,
                  height: 200,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: Color(0xFFF6F1F1)
                    ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.symmetric(horizontal: 12),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            SizedBox(height: 8,),

                            Container(
                              width: 50,
                              height: 50,
                              decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                image: DecorationImage(
                                  image: AssetImage("assets/webinar-person.png"),
                                  fit: BoxFit.cover
                                )
                              ),
                            ),

                            SizedBox(height: 8,),

                            Text("Flutter Expert",
                              style: TextStyle(
                                color: Color(0xFF000000),
                                fontSize: 12,
                                fontWeight: FontWeight.bold
                              ),
                            ),

                            SizedBox(height: 9,),

                            Text("10 Oktober 2020", 
                              style: TextStyle(
                                color: Color(0xFFEB3636),
                                fontSize: 12,
                                fontWeight: FontWeight.w400
                              ),
                            ),

                            SizedBox(height: 4,),

                            Row(
                              children: <Widget>[
                                Icon(Icons.timer, size: 16,),
                                Text("10:00 - 12:00", 
                                  style: TextStyle(
                                    color: Color(0xFF999292),
                                    fontSize: 12,
                                    fontWeight: FontWeight.w400
                                  ),
                                ),
                              ],
                            ),

                            SizedBox(height: 4,),

                            Row(
                              children: <Widget>[
                                Icon(Icons.label_outline, size: 16,),
                                Text("Rp. 100.000", 
                                  style: TextStyle(
                                    color: Color(0xFF999292),
                                    fontSize: 12,
                                    fontWeight: FontWeight.w400
                                  ),
                                ),
                              ],
                            ),

                            SizedBox(height: 12,),

                            Text("Register",
                              style: TextStyle(
                                color: Color(0xFFE92E2E),
                                fontSize: 14,
                                fontWeight: FontWeight.bold
                              ),
                            )
                          ],
                        )
                      ),
                    ],
                  ),
                ),
                
              ],
            ),
          ),

          SizedBox(height: 16,),

          // Row(
          //   children: <Widget>[
          //     Container(
          //       width: 150,
          //       height: 200,
          //       decoration: BoxDecoration(
          //           borderRadius: BorderRadius.circular(10),
          //           color: Colors.red
          //         ),
          //     )
          //   ],
          // ),
        ],
      ),
    );
  }
}
